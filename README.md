# Waterfox #

This is a repository that contains files to help you install waterfox on Linux.

### !!DISCLAIMER!!
This repository has nothing to do with the original waterfox repository.
Sorry for my bad english, i'm hungarian.

### What does waterfox-install do? ###

* Installs up-to-date waterfox versions
* Creates Waterfox commandline shortcut
* Creates Waterfox gui shortcuts in the apps menu

### How do I get set up? ###

* Clone this repository to anywhere on your computer
* Make sure you have the latest python3, tar, and wget installed (if not, then run **sudo apt-get install python3 wget tar**)
* Open up a terminal (On the latest systems CTRL + ALT + T)
* Go to the directory where you cloned this repository
* Run **python3 install.py**
* If you get an error message, let me know

### Thanks to ###

* [These answers](https://askubuntu.com/questions/935466/how-do-i-install-waterfox)
* [The original developer team](https://www.waterfox.net/)
